<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Branch;

/**
 * Class BranchTransformer
 */
class BranchTransformer extends TransformerAbstract
{

    /**
     * Transform the \Branch entity
     * @param \Branch $model
     *
     * @return array
     */
    public function transform(Branch $model,$type='')
    {
        $data=[
          //  'id'         => $model->_id,
            'dia chi' => $model->address,
            'created_at' => $model->created_at->format('y-m-d H::m:s'),
            'updated_at' => $model->updated_at->format('y-m-d H::m:s')
        ];

        if($type == 'for-list'){
           $data['id']=$model->_id;
            return $data;
        }
        if($type == 'for-detail'){
            return [
            'id'         => $model->_id,
            'dia chi' => $model->address,
            'created_at' => $model->created_at->format('y-m-d H::m:s'),
            'updated_at' => $model->updated_at->format('y-m-d H::m:s')
        ];
        }
        return [];
        
    }
}
