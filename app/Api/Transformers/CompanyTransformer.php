<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\Company;

/**
 * Class CompanyTransformer
 */
class CompanyTransformer extends TransformerAbstract
{

    /**
     * Transform the \Company entity
     * @param \Company $model
     *
     * @return array
     */
    public function transform(Company $model)
    {
        return [
            'id'         => $model->_id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
