<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;
use App\Api\Entities\ShiftWork;

/**
 * Class ShiftWorkTransformer
 */
class ShiftWorkTransformer extends TransformerAbstract
{

    /**
     * Transform the \ShiftWork entity
     * @param \ShiftWork $model
     *
     * @return array
     */
    public function transform(ShiftWork $model)
    {
        return [
            'id'         => $model->_id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
