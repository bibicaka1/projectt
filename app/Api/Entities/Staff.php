<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\StaffTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Staff extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'Staff';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform($type='')
    {
        $transformer = new StaffTransformer();

        return $transformer->transform($this,$type);
    }

}
