<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\TitleTransformer;
use Moloquent\Eloquent\SoftDeletes;

class Title extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'Title';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new TitleTransformer();

        return $transformer->transform($this);
    }

}
