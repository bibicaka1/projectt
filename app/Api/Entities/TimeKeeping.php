<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\TimeKeepingTransformer;
use Moloquent\Eloquent\SoftDeletes;

class TimeKeeping extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'TimeKeeping';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform($type='',$data1)
    {
        $transformer = new TimeKeepingTransformer();

        return $transformer->transform($this,$type,$data1);
    }

    public function getCheckInOut(){
        
    }
}
