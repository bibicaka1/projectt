<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\Branch;
//use App\Api\Criteria\Branch;
/**
 * Interface BranchRepository
 */
interface BranchRepository extends RepositoryInterface
{
    public function getBranch($params = [],$limit = 0);
    
}

