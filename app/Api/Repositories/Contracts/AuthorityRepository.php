<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\Authority;
/**
 * Interface AuthorityRepository
 */
interface AuthorityRepository extends RepositoryInterface
{
    
}
