<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\ShiftWork;
/**
 * Interface ShiftWorkRepository
 */
interface ShiftWorkRepository extends RepositoryInterface
{
    
}
