<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\Department;
/**
 * Interface DepartmentRepository
 */
interface DepartmentRepository extends RepositoryInterface
{
    
}
