<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Api\Entities\Title;
/**
 * Interface TitleRepository
 */
interface TitleRepository extends RepositoryInterface
{
    
}
