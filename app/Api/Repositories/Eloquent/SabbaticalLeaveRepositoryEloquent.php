<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\SabbaticalLeaveRepository;
use App\Api\Entities\SabbaticalLeave;
use App\Api\Validators\SabbaticalLeaveValidator;

/**
 * Class SabbaticalLeaveRepositoryEloquent
 */
class SabbaticalLeaveRepositoryEloquent extends BaseRepository implements SabbaticalLeaveRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SabbaticalLeave::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
