<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\ShiftWorkRepository;
use App\Api\Entities\ShiftWork;
use App\Api\Validators\ShiftWorkValidator;

/**
 * Class ShiftWorkRepositoryEloquent
 */
class ShiftWorkRepositoryEloquent extends BaseRepository implements ShiftWorkRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ShiftWork::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
