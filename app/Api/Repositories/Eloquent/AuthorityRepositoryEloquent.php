<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\AuthorityRepository;
use App\Api\Entities\Authority;
use App\Api\Validators\AuthorityValidator;

/**
 * Class AuthorityRepositoryEloquent
 */
class AuthorityRepositoryEloquent extends BaseRepository implements AuthorityRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Authority::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
