<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\TitleRepository;
use App\Api\Entities\Title;
use App\Api\Validators\TitleValidator;

/**
 * Class TitleRepositoryEloquent
 */
class TitleRepositoryEloquent extends BaseRepository implements TitleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Title::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
