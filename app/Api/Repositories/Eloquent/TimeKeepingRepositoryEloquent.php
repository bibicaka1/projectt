<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\TimeKeepingRepository;
use App\Api\Entities\TimeKeeping;
use App\Api\Validators\TimeKeepingValidator;
use App\Api\Criteria\TimeKeepingCriteria;

/**
 * Class TimeKeepingRepositoryEloquent
 */
class TimeKeepingRepositoryEloquent extends BaseRepository implements TimeKeepingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TimeKeeping::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
     public function getTimeKeeping($params = [],$limit = 0) 
    {
        $this->pushCriteria(new TimeKeepingCriteria($params));
        if(!empty($params['is_detail'])) {
            $item = $this->get()->first();
        } elseif(!empty($params['is_paginate'])) {
            $item = $this->paginate();  
        } else {
            $item = $this->all(); 
        }
        $this->popCriteria(new TimeKeepingCriteria($params));
        return $item;
    }
}
