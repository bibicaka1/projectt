<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\TitleRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Title;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class TitleController extends Controller
{

    protected $userRepository;
    protected $titleRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        UserRepository $userRepository,
        TitleRepository $titleRepository,
        AuthManager $auth,
        Request $request
    ) {
        $this->userRepository = $userRepository;
        $this->titleRepository = $titleRepository;
        $this->request = $request;
        $this->auth = $auth;
        parent::__construct();
    }

    public function create()
    {
        $validator1 = \validator::make($this->request->all(),[
            'name'=>'required',
        ]);
        if($validator1->fails())
        {
            return $this->errorBadRequest($validator1->messages()->toArray());
        }
        $name = $this->request->get('name');
        // $address = $this->request->get('address');
         

        $attributes = [
            'name' => $this->request->get('name')
        ];

        $title =$this->titleRepository->create($attributes);
        return $this->successRequest($title);
    }
     public function update()
    {
        $validator1 = \validator::make($this->request->all(),[
            '_id'=>'required',
            'name'=>'required',
        ]);
        // Input
        $id = $this->request->get('_id');
        // Delete
        $title= Title::where('_id', mongo_id($id))->first();
        if (!empty($title)) 
        {
            if(!empty($this->request->get('name')))
            {
                $title->name=$this->request->get('name');
            }
            $title->save();
        }
         return $this->successRequest($title);
    }
    public function delete()
    {
         $validator1 = \validator::make($this->request->all(),[
            '_id'=>'required',
        ]);
        $id=$this->request->get('_id');
        $title=Title::where(['_id',mongo_id($id)])->first();
        if(!empty($title))
        {   
            $title->delete();
        }
        return $this->successRequest();
    }
    public function view()
    {
        $title = Title::all();
        return $this->successRequest($title);
    }
}