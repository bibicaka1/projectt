<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\ShiftWorkRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\ShiftWork;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class ShiftWorkController extends Controller
{
	protected $userRepository;
	protected $shiftworkRepository;
	protected $auth;
    protected $request;

    public function __construct(   
    	UserRepository $userRepository,
        ShiftWorkRepository $shiftworkRepository,
        AuthManager $auth,
        Request $request
    ) {
        $this->userRepository = $userRepository;
        $this->shiftworkRepository = $shiftworkRepository;
        $this->request = $request;
        $this->auth = $auth;
        parent::__construct();
    }


	public function create()
    {
		$validator = \validator::make($this->request->all(),[
			
            'time_start'=>'required',
            'time_end' =>'required',
			'id_staff'=>  'required',
		]);
		
        if($validator->fails()){
			return $this->errorBadRequest($validator->messages()->toArray());
		}
        $time_start=$this->request->get('time_start');
        $time_end= $this->request->get('time_end');
		$attributes = [                 
            'time_start'=>$this->request->get('time_start'),
            'time_end'=>$this->request->get('time_end'),
            'id_staff' => $this->request->get('id_staff')
		];  
		
        $shiftwork =$this->shiftworkRepository->create($attributes);
        
        return $this->successRequest($shiftwork);
	}
	
    public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            'times_start'=>'required',
            'time_end'=>'required', 
            'id_staff'=>  'required',
            '_id' =>'required',
        ]);

        $id = $this->request->get('_id');
        $shiftwork = ShiftWork::where([
            '_id'=> mongo_id($id),
        ])->first();
        
        if (!empty($shiftwork)) {
           //  if(!empty($this->request->get('timein'))){$nhanvien1->name=$this->request->get('timein');}
            if(!empty($this->request->get('time_start')))
            {
                $shiftwork->time_start=$this->request->get('time_start');
            }
            if(!empty($this->request->get('time_end')))
            {
                $shiftwork->time_end=$this->request->get('time_end');
            }
            
            if(!empty($this->request->get('id_staff')))
            {
                $shiftwork->id_staff=$this->request->get('id_staff');
            }
           
            $shiftwork->save();
        }
        
        return $this->successRequest($shiftwork);
    }
    
    public function delete()
    {
         $validator = \validator::make($this->request->all(),[
            '_id' =>'required',
        ]);
    	$id=$this->request->get('_id');  	
    	$shiftwork=ShiftWork::where('_id',mongo_id($id))->first();
    	if(!empty($shiftwork))
        {
    		$shiftwork->delete();
    	}
    	return $this->successRequest(trans('core.success'));
    }
    public function view()
    {
        ('aaaaa');
    	$shiftwork = ShiftWork::all();
       	$viewData = [
            'ShiftWork' => $shiftwork
        ];
        return view('ShiftWork-list',$viewData);
    }

   
}