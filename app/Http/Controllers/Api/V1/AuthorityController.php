<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\AuthorityRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Authority;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class AuthorityController extends Controller
{
    protected $userRepository;
    protected $authorityRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        UserRepository $userRepository,
        AuthorityRepository $authorityRepository,
        AuthManager $auth,
        Request $request
    ) {
        $this->userRepository = $userRepository;
        $this->authorityRepository = $authorityRepository;
        $this->request = $request;
        $this->auth = $auth;
        parent::__construct();
    }


    public function createAuthority(){

        
        $validator = \validator::make($this->request->all(),[
            'name'=>'required',
          
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }
       
        $name = $this->request->get('name');
        // $address = $this->request->get('address');
         

        $attributes = [
           
            'name' => $this->request->get('name')
        
        ];

         $authority =$this->authorityRepository->create($attributes);
        return $this->successRequest($authority);
    }
     public function updateAuthority() {
        // Input

        $id = $this->request->get('_id');
    
        // Delete
       $authority  = Authority::where('_id', mongo_id($id))->first();
        if (!empty($authority)) {

           
            if(!empty($this->request->get('name')))
            {
                $authority->name=$this->request->get('name');
            }
            // if(!empty($this->request->get('address')))
            //     {
            //         $quyenhan1->email=$this->request->get('address');
            //     }
            // if(!empty($this->request->get('phone')))
            // {
            //     $quyenhan1->phone=$this->request->get('phone');
            // }
            
            $authority->save();
        }
         return $this->successRequest($authority);
       
    }
    public function deleteAuthority()
    {

        $id=$this->request->get('_id');
        $authority=Authority::where('_id',mongo_id($id))->first();
        if(!empty($authority))
        {
            $authority->delete();
        }
        return $this->successRequest();
    }
    public function viewAuthority()
    {
          $authority = Authority::all();
       return $this->successRequest($authority);
    }
}