<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\StaffRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Staff;
use App\Api\Entities\User;
use Illuminate\View\View;

//Google firebase
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Support\Facades\Auth;


class StaffController extends Controller
{
	protected $userRepository;
	protected $staffRepository;
	protected $auth;
    protected $request;

    public function __construct(   
    	UserRepository $userRepository,
        StaffRepository $staffRepository,
        AuthManager $auth,
        Request $request
    ) {
        $this->userRepository = $userRepository;
        $this->staffRepository = $staffRepository;
        $this->request = $request;
        $this->auth = $auth;
        parent::__construct();
    }


	public function create(){
        
		$validator = \validator::make($this->request->all(),[
			//'name'=>'required',
			//'accountlogin'=>'required',
			'name'=>'required',
			'mail'=>'required',
			'phone'=>'required',
            'id_department'=>'required',
            'id_title'=>'required',
            'id_company'=>'required',

		]);
		if($validator->fails()){
			return $this->errorBadRequest($validator->messages()->toArray());
		}
	//	$nameshop = $this->request->get('nameshop');
		//$accountlogin = $this->request->get('accountlogin');
		$name = $this->request->get('name');
		$mail = $this->request->get('mail');
		$phone = $this->request->get('phone');
        $id_department = $this->request->get('id_department');
        $id_title = $this->request->get('id_title');
        $id_company = $this->request->get('id_company');


		$attributes = [
		//	'nameshop'=> $this->request->get('nameshop'),
			//'accountlogin'=> $this->request->get('accountlogin'),
			'name' => $this->request->get('name'),
			'mail' => $this->request->get('mail'),
			'phone' => $this->request->get('phone'),
            'id_department' => $this->request->get('id_department'),
            'id_title' => $this->request->get('id_title'),
            'id_company' => $this->request->get('id_company')
		];

		$staff =$this->staffRepository->create($attributes);
        return $this->successRequest($staff);
	}
	 public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            //'name'=>'required',
            //'accountlogin'=>'required',
            'name'=>'required',
            'mail'=>'required',
            'phone'=>'required',
            'id_department'=>'required',
            'id_title'=>'required',
            'id_company'=>'required',

        ]);
        $id = $this->request->get('_id');     
        $staff = NhanVien::where([
            '_id'=> mongo_id($id),
        ])->first();
        if (!empty($staff)) {
            if(!empty($this->request->get('name')))
            {
                $staff->name=$this->request->get('name');
            }
            if(!empty($this->request->get('mail')))
            {
                $staff->mail=$this->request->get('mail');
            }
            if(!empty($this->request->get('phone')))
            {
                $staff->phone=$this->request->get('phone');
            }
            if(!empty($this->request->get('id_department')))
            {
                $staff->id_department=$this->request->get('id_department');
            }
             if(!empty($this->request->get('id_title')))
            {
                $staff->id_title=$this->request->get('id_title');
            }
         
             if(!empty($this->request->get('id_company')))
            {
                $staff->id_company=$this->request->get('id_company');
            }
           
            $staff->save();
        }
         return $this->successRequest($staff);
    }
    public function delete()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required',

        ]);
    	$id=$this->request->get('_id');  	
    	$staff=Staff::where('_id',mongo_id($id))->first();
    	if(!empty($staff))
    	{
    		
    		$staff->delete();
    	}
    	return $this->successRequest(trans('core.success'));
    }
    public function view()
    {
    	  $staff = Staff::all();
       	$viewData = [
            'Staff' => $staff
        ];
        
        return view('Staff-list',$viewData);
    }
}