<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('Test/register', [
            'as' => 'Test.register',
            'uses' => 'TestController@create',
        ]);
         $api->post('Test/update', [
            'as' => 'Test.update',
            'uses' => 'TestController@update',
        ]);
          $api->post('Test/delete', [
            'as' => 'Test.delete',
            'uses' => 'TestController@delete',
        ]);
           $api->post('Test/view', [
            'as' => 'Test.view',
            'uses' => 'TestController@view',
        ]);
             $api->post('Test/go', [
            'as' => 'Test.go',
            'uses' => 'TestController@go',
        ]);
             $api->post('Test/mail', [
            'as' => 'Test.mail',
            'uses' => 'TestController@mail',
        ]);
    });
        


});
