<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('Company/register', [
            'as' => 'Company.register',
            'uses' => 'CompanyController@create',
        ]);
         $api->post('Company/update', [
            'as' => 'Company.update',
            'uses' => 'CompanyController@update',
        ]);
          $api->post('Company/delete', [
            'as' => 'Company.delete',
            'uses' => 'CompanyController@delete',
        ]);
           $api->post('Company/view', [
            'as' => 'Company.view',
            'uses' => 'Companyontroller@view',
        ]);
    });
        


});
