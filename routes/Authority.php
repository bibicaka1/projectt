<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('Authority/register', [
            'as' => 'Authority.register',
            'uses' => 'AuthorityController@createAuthority',
        ]);
        $api->post('Authority/update', [
            'as' => 'Authority.update',
            'uses' => 'AuthorityController@updateAuthority',
        ]);
        $api->post('QuyenHan/delete', [
            'as' => 'QuyenHan.delete',
            'uses' => 'QuyenHanController@deleteAuthority',
        ]);
        $api->post('Authority/view', [
            'as' => 'Authority.view',
            'uses' => 'AuthorityController@viewAuthority',
        ]);
    });
        


});
