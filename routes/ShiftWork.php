<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('ShiftWork/register', [
            'as' => 'ShiftWork.register',
            'uses' => 'ShiftWorkController@create',
        ]);
         $api->post('ShiftWork/update', [
            'as' => 'ShiftWork.update',
            'uses' => 'ShiftWorkController@update',
        ]);
          $api->post('ShiftWork/delete', [
            'as' => 'ShiftWork.delete',
            'uses' => 'ShiftWorkController@delete',
        ]);
           $api->post('ShiftWork/view', [
            'as' => 'ShiftWork.view',
            'uses' => 'ShiftWorkController@view',
        ]);
           $api->post('ShiftWork/Test', [
            'as' => 'ShiftWork.Test',
            'uses' => 'ShiftWorkController@Test',
        ]);

    });
        


});
